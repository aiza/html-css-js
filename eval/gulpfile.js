'use strict';

var gulp = require ('gulp'),
	sass = require ('gulp-sass'),
	watch = require('gulp-watch'),
	browserSync = require('browser-sync').create(),
    concat = require('gulp-concat'),
    minify = require('gulp-minify'),
    pump = require('pump'),
    uglify = require('gulp-uglify'),
    cleanCSS = require('gulp-clean-css'),
    htmlmin = require('gulp-htmlmin');
    

gulp.task('sass', function () {
  return gulp.src('./src/scss/main.scss')
    .pipe(sass())
    .pipe(gulp.dest('./src/css'))
    .pipe(browserSync.reload({
		stream: true
	}));
});


gulp.task('watch',['browser-sync', 'sass','concatJs'], function () {
	gulp.watch ('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/javascript/*.js',['concatJs']);
	gulp.watch('src/*.html', browserSync.reload);
    gulp.watch('src/javascript//*.js', browserSync.reload);
})

 
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./src/"
        }
    });
});    


gulp.task('concatJs', function() {
  return gulp.src([ './src/javascript/libs/jquery-3.2.1.min.js','./src/javascript/*.js'])
    .pipe(concat('production.js'))
    .pipe(gulp.dest('./src/js'));
});


gulp.task('minify-js', function (cb) {
  pump([
        gulp.src('src/js/production.js'),
        uglify(),
        gulp.dest('./dist/js/')
    ],
    cb
  );
});


gulp.task('minify-css', () => {
  return gulp.src('./src/css/main.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./dist/css/'));
});


gulp.task('minify-html', function(){
    gulp.src('./src/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('./dist/'));
});


gulp.task('copy',function(){
    gulp.src('./src/assets/img/*.*')
        .pipe(gulp.dest('./dist/assets/img'));
    gulp.src('./src/fonts/**/*.*')
        .pipe(gulp.dest('./dist/fonts'));
});


gulp.task('build',['minify-js','minify-css', 'minify-html','copy' ]);
